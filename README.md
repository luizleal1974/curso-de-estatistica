# Curso de Estatística

<div align="justify">

<p>Este repositório contém os arquivos que serão utilizados no curso de estatística básica. Acese o livro eletrônico <a target='_blank' rel='noopener noreferrer' href='https://statisticalmetrology.shinyapps.io/basicstat/'><b>Estatística Básica</b></a> para informações complementares. Os códigos de progamação podem ser executados conforme abaixo:</p>

</div>

<p>
&#x2022; <a target='_blank' rel='noopener noreferrer' href='https://posit.cloud/'><b>Posit Cloud</b></a> <code><b>(R)</b></code>

&#x2022; <a target='_blank' rel='noopener noreferrer' href='https://colab.research.google.com/drive/1IGVHOY4t8orJ2e_SWlM597pWyu7zgy8z?usp=sharing'><b>Google Colab</b></a> <code><b>(R)</b></code>

&#x2022; <a target='_blank' rel='noopener noreferrer' href='https://colab.research.google.com/drive/1nguaDatjq7hCXXGD15PC2rsvfgGk6jyv?usp=sharing'><b>Google Colab</b></a> <code><b>(Python)</b></code>
</p>


<div align="justify">

<p>Para comentários, dúvidas e sugestões entre em contato por meio do correio eletrônico <a href="mailto:statistical.metrology@gmail.com"><b>statistical.metrology@gmail.com</b></a>.</p>

</div>

</br>

</br>

<div align="center"><img src="Estatistica_Basica.gif"/></div>

